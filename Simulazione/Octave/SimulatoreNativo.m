close all
%clear all
graphics_toolkit('gnuplot')
pkg load control
source("Git/SchedaSupercap/Simulazione/Octave/Identificazione.m")

function [Y T sys err tau]=getAndPlotSystem(t,v,c1,cv,r1,m,maxK)
  [c r tau]=identifyOther(t,v,c1,cv,r1,m,maxK)
  c(1)=((v(1)*cv+c1)+(v(end)*cv+c1)/2);
  [A B C D]=getSystem(c,r,tau);
  sys=ss(A,B,C,D);
  close all
  [Y,T,X]=lsim(sys,zeros(1,length(t)),t,[v(1) zeros(1,maxK-1)]);
  for i=1:length(Y)
    err(i)=Y(i)-v(i);
  end
  plot(T,Y,'b'), ylabel("Volt"),  xlabel("Secondi"), title("Evoluzione Libera")
  hold on, plot(t,v,'g');
  plot(t,err,'r');
  plot(tau,v(1)*ones(1,length(tau)),'*k'), legend({"Simulazione","Dati","Errore"},"location","west");
endfunction

function [Y T y ty err err2]=plotDifferences(trimt,trimv,trimt2,trimv2,M,branch,tcharge)
%Get system
  figure(1)
  [Y T sys err tau]=getAndPlotSystem(trimt,trimv,5.78,50,.4,M,branch);
  title("DLC5CapA-LPF vs Sys"),legend({"Simulazione","Dati","Errore"},"location","west");
  saveas(1,["Git/SchedaSupercap/Simulazione/Dati/Grafici/graficoLSYS_M" num2str(M) "B" num2str(branch) ".png"]);

%Simulate prediction
  maxV=trimv2(1);
  x(1)=maxV;
  for i=2:branch
    x(i)=maxV-maxV*exp(-tcharge/tau(i))
  end
  [y ty xy]=lsim(sys,zeros(1,length(trimt2)),trimt2,x);

%Calculate errors
  for i=1:length(y)
    err2(i)=y(i)-trimv2(i);
  end
    
%Plot prediction and metadata
  figure(2)
  plot(ty,y,'m')
  hold on, plot(trimt2,trimv2,'g')
  plot(trimt2,err2,'r');
  title("DLC6CapA-LPF vs Sys"),legend({"Simulazione","Dati","Errore"},"location","west");
  saveas(2,["Git/SchedaSupercap/Simulazione/Dati/Grafici/graficoLSIM_M" num2str(M) "B" num2str(branch) ".png"]);
endfunction

set1=load("Git/SchedaSupercap/Simulazione/Dati/Filtrati/esperimentoTrimDLC5CapA.mat");
set2=load("Git/SchedaSupercap/Simulazione/Dati/Filtrati/esperimentoTrimDLC6CapA.mat");
trimt=set1(1,:);
trimv=set1(2,:);
trimt2=set2(1,:);
trimv2=set2(2,:);


m=10;
b=4;
tcharge=355.38;
[Y T y ty e1 e2]=plotDifferences(trimt,trimv,trimt2,trimv2,10,4,tcharge);

save ("Git/SchedaSupercap/Simulazione/Dati/Elaborati/NATIVO_ID-T.mat","T","Y","-ascii");
save ("Git/SchedaSupercap/Simulazione/Dati/Elaborati/NATIVO_SIM-T.mat","ty","y","-ascii");
