close all
clear all
graphics_toolkit('gnuplot')
pkg load control

%Load raw data and filter them through a second order low pass filter
function [tc v a]=loadAndFilter(filename)
  M=load(filename);

  t=M(1,:);
  volt=M(2,:);
  amp=M(3,:);
  T=t(2);

  alpf = tf([1/(T*4)],[1 1/(T*4)]);
  lpf = c2d(alpf*alpf,T);
  lpv = lsim(lpf,volt,t);
  lpa = lsim(lpf,amp,t);

  for i=1:(length(lpv)-1)/2
    v(i)=lpv(i*2);
    a(i)=lpa(i*2);
    tc(i)=t(i*2)-t(2);
  end
endfunction

%Trim a specific interval in a set of data
function [t v a]=trim(t,v,a,t1,t2)
  i1=find(t>t1)(1);
  i2=find(t>t2)(1);
  t=t(i1:i2).-t(i1);
  v=v(i1:i2);
  a=a(i1:i2);
endfunction

%Get data for identification
t1=445.2; t2=67635;
[tc v a]=loadAndFilter("Git/SchedaSupercap/Simulazione/Dati/Grezzi/esperimentoDLC5CapA.mat");
[trimt trimv]=trim(tc,v,a,t1,t2);

%Get data for comparison
t0=49.2; t1=3603; t2=83459; tcharge=t1-t0;
[tc v a]=loadAndFilter("Git/SchedaSupercap/Simulazione/Dati/Grezzi/esperimentoDLC6CapA.mat");
[trimt2 trimv2]=trim(tc,v,a,t1,t2);

%Store processed data for further elaborations
save("Git/SchedaSupercap/Simulazione/Dati/Filtrati/esperimentoTrimDLC5CapA.mat","trimt","trimv","-ascii");
save("Git/SchedaSupercap/Simulazione/Dati/Filtrati/esperimentoTrimDLC6CapA.mat","trimt2","trimv2","-ascii");
