//load with MINI-CORE board configurations: https://www.circuito.io/blog/atmega328p-bootloader/

#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>

//========= PHISICAL CONFIGURATIONS===========	
const byte pmos=9;
const byte nmos=6;
const byte led=5;

// vinSense - CsupSense = I*Rsense 
// CsupSense = Vcc
const float Rsense=2.5;
const byte VinSense=A0;  //impossible reading because Vin is always greater than Vcc
const byte CsupSense=A7; //redundant
const byte CinfSense=A6;
//=============================================

typedef struct{
  double vcc;
  double vCsup;
  double vCinf;  
  double vIsense;
  double I;
} State;  
  

// https://www.intorobotics.com/how-to-make-accurate-adc-readings-with-arduino/
double readVcc() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1125300L / result; // Back-calculate AVcc in mV
  return (double) result/1000.0;
}

// https://gammon.com.au/adc
double readVoltage(int analogPin, double vccVal){
  return ( (double)analogRead(analogPin)+.5 ) / 1024.0 * vccVal; 
}

// https://gammon.com.au/power
ISR (WDT_vect) 
{
   wdt_disable();  // disable watchdog
}
void power_down(){
 // disable ADC
  ADCSRA = 0;  

  // clear various "reset" flags
  MCUSR = 0;     
  // allow changes, disable reset
  WDTCSR = bit (WDCE) | bit (WDE);
  // set interrupt mode and an interval 
  WDTCSR = bit (WDIE) | bit (WDP3) | bit (WDP0);    // set WDIE, and 8 seconds delay
  wdt_reset();  // pat the dog
  
  set_sleep_mode (SLEEP_MODE_PWR_DOWN);  
  noInterrupts ();           // timed sequence follows
  sleep_enable();
  interrupts ();
  sleep_cpu ();

  // cancel sleep as a precaution
  sleep_disable();
}

void sampleState(State * const s){
    s->vcc=readVcc();
    s->vIsense=readVoltage(VinSense,s->vcc);	
    s->vCinf=readVoltage(CinfSense,s->vcc);
    s->vCsup=s->vcc - s->vCinf;
    s->I=(s->vIsense - s->vcc) / Rsense;
    return;
}

void printVal (const char * const label,const double val){
   Serial.print(label);
   Serial.println(val);
}

void dumpState(const State * const  s){
    Serial.println("=== State Dump ===");
    printVal("Csup: ",s->vCsup);
    printVal("Cinf: ",s->vCinf);
    printVal("I: ",s->I);
    printVal("Vin: ",s->vIsense);
    printVal("Vcc: ",s->vcc);
    Serial.println("==================");
}

void flash(){
    digitalWrite (led,HIGH);
    delay(50);
    digitalWrite(led,LOW);
    delay(50);
}

void protectCap(double val, int outPin, double threshold, byte signalEnable){
  if( val > threshold ){
    digitalWrite(outPin,signalEnable);
    delay(500);
    Serial.println(outPin);
  }
  else  
    digitalWrite(outPin,!signalEnable);
}

void setup() {
  pinMode(nmos,OUTPUT);
  pinMode(pmos,OUTPUT);
  pinMode(led,OUTPUT);
  digitalWrite(nmos,LOW);
  digitalWrite(pmos,HIGH);
  Serial.begin(9600);
}

void loop() {
  static State st;

  sampleState(&st);

  //debug
  flash();
  dumpState(&st); 

  protectCap(st.vCsup, pmos, 2, LOW);  //p-channel
  protectCap(st.vCinf, nmos, 2, HIGH); //n-channel

  //if(st.I>0){ //active mode: steady light
  if(digitalRead(2)==LOW){ //can't sense the current, use manual signal instead
    //digitalWrite(led,HIGH);
    delay(500);
  }
  else{ //passive mode: sleep
    digitalWrite(led,LOW);
    digitalWrite(nmos,LOW);
    digitalWrite(pmos,HIGH);
    //power_down();
  }
}
