close all
clear all
graphics_toolkit('gnuplot')
pkg load control

rawData1=load("Git/SchedaSupercap/Simulazione/Dati/Filtrati/esperimentoTrimDLC5CapA.mat");
t1=rawData1(1,:);
v1=rawData1(2,:);

rawData2=load("Git/SchedaSupercap/Simulazione/Dati/Filtrati/esperimentoTrimDLC6CapA.mat");
t2=rawData2(1,:);
v2=rawData2(2,:);

DataID1=load("Git/SchedaSupercap/Simulazione/Dati/Elaborati/NATIVO_ID-T.mat");
id1=DataID1(2,:);

DataSIM1=load("Git/SchedaSupercap/Simulazione/Dati/Elaborati/NATIVO_SIM-T.mat");
sim1=DataSIM1(2,:);

DataID2=load("Git/SchedaSupercap/Simulazione/Dati/Elaborati/CUST_ID-T.mat");
id2=DataID2(2,:);

DataSIM2=load("Git/SchedaSupercap/Simulazione/Dati/Elaborati/CUST_SIM-T.mat");
sim2=DataSIM2(2,:);

figure(1);
plot(t1,v1,'k'),hold on
plot(t1,id1,'g'),title("Dati identificazione vs lsim vs Simulatore ad hoc");
plot(t1,id2,'m'),legend({"Dati","lsim","Simulatore ad hoc"},"location","west");
saveas(1,"Git/SchedaSupercap/Simulazione/Dati/Grafici/CompID.png");

figure(2);
plot(t2,v2,'k'),hold on
plot(t2,sim1,'g'),title("Dati vs lsim vs Simulatore ad hoc");
plot(t2,sim2,'m'),legend({"Dati","lsim","Simulatore ad hoc"},"location","northeast");
saveas(2,"Git/SchedaSupercap/Simulazione/Dati/Grafici/CompSIM.png");
