close all
%clear all
graphics_toolkit('gnuplot')
pkg load control
source("Git/SchedaSupercap/Simulazione/Octave/Identificazione.m")

function [Y Y2 err tau]=customSimulator(t,v,t2,v2,c1,cv,r1,m,maxK,icond)
  [c r tau]=identifyOther(t,v,c1,cv,r1,m,maxK)
  T=t(2)-t(1);
  id=eye(maxK);
  
  Y(1)=v(1);
  state=[v(1); zeros(maxK-1,1)];

  for k=2:length(t)
    c(1)=c1+cv*Y(k-1);
    [A B C D]=getSystem(c,r,tau);
    A0=A*T/2;
    A2=inv(id-A0);
    A1=(id+A0)*A2;
    state=A1*state;
    Y(k)=C*state;
  endfor
  
  for i=1:length(Y)
    err(i)=Y(i)-v(i);
  end

  figure(1)
  plot(t(1:length(Y)),Y,'b'),xlabel("Secondi"),ylabel("Volt"),title("Evoluzione Libera");
  hold on, plot(t,v,'g');
  plot(t(1:length(Y)),err,'r');
  plot(tau,v(1)*ones(1,length(tau)),'*k'),legend({"Simulazione","Dati","Errore"},"location","west");
  saveas(1,["Git/SchedaSupercap/Simulazione/Dati/Grafici/graficoID_M" num2str(m) "B" num2str(maxK) ".png"]);

  Y2(1)=v2(1);
  state=icond;

  for k=2:length(t2)
    c(1)=c1+cv*Y2(k-1);
    [A B C D]=getSystem(c,r,tau);
    A0=A*T/2;
    A2=inv(id-A0);
    A1=(id+A0)*A2;
    state=A1*state;
    Y2(k)=C*state;
  endfor
  
  for i=1:length(Y2)
    err2(i)=Y2(i)-v2(i);
  end

  figure(2)
  plot(t2(1:length(Y2)),Y2,'b'),xlabel("Secondi"),ylabel("Volt"),title("Evoluzione Libera");
  hold on, plot(t2,v2,'g');
  plot(t2(1:length(Y2)),err2,'r'),legend({"Simulazione","Dati","Errore"},"location","west");
  saveas(2,["Git/SchedaSupercap/Simulazione/Dati/Grafici/graficoSIM_M" num2str(m) "B" num2str(maxK) ".png"]);
endfunction  

set1=load("Git/SchedaSupercap/Simulazione/Dati/Filtrati/esperimentoTrimDLC5CapA.mat");
set2=load("Git/SchedaSupercap/Simulazione/Dati/Filtrati/esperimentoTrimDLC6CapA.mat");

trimt=set1(1,:);
trimv=set1(2,:);
trimt2=set2(1,:);
trimv2=set2(2,:);

branch=4;
M=10;

[Y1 Y2 e1 tau]=customSimulator(trimt,trimv,trimt2,trimv2,5.78,50,.4,M,branch,[2.742866;2.284812;0.259608;0.015110]);

save ("Git/SchedaSupercap/Simulazione/Dati/Elaborati/CUST_ID-T.mat","trimt","Y1","-ascii");
save ("Git/SchedaSupercap/Simulazione/Dati/Elaborati/CUST_SIM-T.mat","trimt2","Y2","-ascii");

% DLC5 - DLC6
% |  M | B |  E1 |  E2 |  
% |----+---+-----+-----|
% |  3 | 8 | .23 | .27 |
% |  4 | 7 | .18 | .25 |
% |  5 | 6 | .20 | .26 |
% |  8 | 5 | .19 | .25 |
% | 18 | 4 | .19 | .25 |
% | 78 | 3 | .47 | .34 |
% |  8 | 4 | .39 | .31 |
% | 11 | 4 | .28 | .27 |
