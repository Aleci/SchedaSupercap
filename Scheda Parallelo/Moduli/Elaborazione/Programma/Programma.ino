//load with MINI-CORE board configurations: https://www.circuito.io/blog/atmega328p-bootloader/
	
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>

const byte Led = 5;

void routine(){
  pinMode(Led,OUTPUT);
  digitalWrite (Led,HIGH);
  delay(50);
  digitalWrite(Led,LOW);
  delay(50);
  pinMode(Led,INPUT);
}
  
// watchdog interrupt
ISR (WDT_vect) 
{
   wdt_disable();  // disable watchdog
}
 
void setup(){
  for(byte i=0;i<A5;i++)
    pinMode(i,INPUT);
}

// https://gammon.com.au/power
void loop(){
  routine();

  // disable ADC
  ADCSRA = 0;  

  // clear various "reset" flags
  MCUSR = 0;     
  // allow changes, disable reset
  WDTCSR = bit (WDCE) | bit (WDE);
  // set interrupt mode and an interval 
  WDTCSR = bit (WDIE) | bit (WDP3) | bit (WDP0);    // set WDIE, and 8 seconds delay
  wdt_reset();  // pat the dog
  
  set_sleep_mode (SLEEP_MODE_PWR_DOWN);  
  noInterrupts ();           // timed sequence follows
  sleep_enable();
  interrupts ();
  sleep_cpu ();

  // cancel sleep as a precaution
  sleep_disable();
  
} 
