typedef struct{
  double *C;
  double *R;
  double Cv;
  int b; //number of branch of the system
}Sys;
typedef Sys* Param;

typedef struct{
  double * vector;
  int length;
  int index;
}TelescopicArray;

TelescopicArray* newTelescopicArray(){
  TelescopicArray *a = (TelescopicArray*) malloc(sizeof(TelescopicArray));
  if(a==NULL)
    fprintf(stderr,"Unable to create array\n");
  a->vector=calloc(100,sizeof(double));
  if(a->vector == NULL)
    fprintf(stderr,"Unable to allocate vector\n");
  a->length=100;
  a->index=0;
  return a;
}

void append(TelescopicArray* a,double val){
  if(a->index >= a->length){
    a->vector=realloc(a->vector,a->length * 2 * sizeof(double));
    a->length *= 2;
  }
  if(a->vector == NULL)
    fprintf(stderr,"Unable to realloc vector\n");
    a->vector[a->index]=val;
  (a->index)++;
}

double * detachVector(TelescopicArray *a){
  double * ptr;
  if((ptr = (double*) realloc(a->vector,(a->index+1)*sizeof(double))) == NULL)
    fprintf(stderr,"Unable to realloc vector\n");
  return ptr;
}

int length(TelescopicArray* a){
  return a->index;
}

typedef struct{
  double *t;
  double *v;
  double *a;
  int length;
}MatData;
typedef MatData* Data;

Data getData(FILE *fp){
  TelescopicArray *a;
  Data d = (Data)malloc(sizeof(MatData));
  char c;
  int i=0;
  double val,*ptr;
  a = newTelescopicArray();
  if(d==NULL)
    fprintf(stderr,"Unable to create Data\n");
  do{
    fscanf(fp,"%le%c",&val,&c);
    append(a,val);
  }while(c==' ');
  d->t = detachVector(a);
  d->length = a->index;
  free(a);
  if((ptr = (double*) malloc(sizeof(double)*d->length)) == NULL)
    fprintf(stderr,"Unable to allocate row array\n");
  do{
    fscanf(fp,"%le%c",&val,&c);
    ptr[i++]=val;
  }while(c==' ');
  d->v=ptr;
  if((ptr = (double*) malloc(sizeof(double)*d->length)) == NULL)
    fprintf(stderr,"Unable to allocate row array\n");
  i=0;
  while(fscanf(fp,"%le%c",&val,&c)!=EOF)
    ptr[i++]=val;
  d->a=ptr;
  return d;
}

int sum(Param p,double * V){
  double sum=0;
  for(int i=0; i < p->b; i++)
    sum+=V[i]/p->R[i];
  return sum;
}

double * solve (double * A,double *B,int size){
  gsl_matrix_view m = gsl_matrix_view_array (A, size, size);
  gsl_vector_view b = gsl_vector_view_array (B, size);
  gsl_permutation *p = gsl_permutation_alloc (size);
  gsl_vector *x = gsl_vector_alloc (size);
  double *X;
  int s;
  if((X=(double*)malloc(sizeof(double)*size))==NULL)
    fprintf(stderr,"Unable to allocate solution vector\n");
  
  gsl_linalg_LU_decomp (&m.matrix, p, &s);
  gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x);

  memcpy(X,x->data,sizeof(double)*size);

  gsl_permutation_free (p);
  gsl_vector_free (x);
  return X;
}
