#define DEBUG
#define EMM 18
#define BRANCH 4
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "gnuplot_c.h"
#include <gsl/gsl_linalg.h>

#define MYALLOC(var,type,num,msg) \
  if((var=(type *)malloc(sizeof(type)*num)) == NULL) fprintf(stderr,msg)

typedef struct{
  double *C;
  double *R;
  double Cv;
  double *Tau;
  int b; //number of branch of the system
}Sys;
typedef Sys* Param;

typedef struct{
  double * vector;
  int length;
  int index;
}TelescopicArray;

void printVector(double * vector,int size,char * label){
  int len = strlen(label);
  char space[len];
  for(int k=0; k<len+4 ; k++)
    space[k]=' ';
  printf("%s = [ %.3lf",label,vector[0]);
  for(int k=1; k<size ; k++)
    printf("\n%s%.3lf",space,vector[k]);
  printf(" ]\n");
}

void printGSLmatrix(gsl_matrix * m,int n,char * label){
  //only squared matrix
  printf("%s = [\n",label);
  for(int i=0;i<n;i++)
    for(int j=0;j<n;j++)
      printf("%e%s",gsl_matrix_get(m,i,j),j==n-1 ? ";\n" : ", ");
  printf(" ]\n");
}

void printGSLvector(gsl_vector * v,int n,char * label){
  printf("%s = [\n",label);
  for(int i=0;i<n;i++)
      printf("%e,\n",gsl_vector_get(v,i));
  printf(" ]\n");
}

TelescopicArray* newTelescopicArray(){
  TelescopicArray *a;
  MYALLOC(a,TelescopicArray,1,"Unable to allocate a new Telescopic array\n");
  MYALLOC(a->vector,double,100,"Unable to allocate a new vector\n");
  a->length=100;
  a->index=0;
  return a;
}

void append(TelescopicArray* a,double val){
  if(a->index >= a->length){
    a->vector=realloc(a->vector,a->length * 2 * sizeof(double));
    a->length *= 2;
  }
  if(a->vector == NULL)
    fprintf(stderr,"Unable to realloc vector\n");
    a->vector[a->index]=val;
  (a->index)++;
}

double * detachVector(TelescopicArray *a){
  double * ptr;
  if((ptr = (double*) realloc(a->vector,(a->index+1)*sizeof(double))) == NULL)
    fprintf(stderr,"Unable to realloc vector\n");
  return ptr;
}

int length(TelescopicArray* a){
  return a->index;
}

typedef struct{
  double *t;
  double *v;
  double *a;
  int length;
}MatData;
typedef MatData* Data;

Data getData(FILE *fp){
  TelescopicArray *a;
  Data d;
  char c;
  int i=0;
  double val,*ptr;

  MYALLOC(d,MatData,1,"Unable to create a new Data structure");
  a = newTelescopicArray();
  if(d==NULL)
    fprintf(stderr,"Unable to create Data\n");
  do{
    fscanf(fp,"%le%c",&val,&c);
    append(a,val);
  }while(c==' ');
  d->t = detachVector(a);
  d->length = a->index;
  free(a);
  MYALLOC(ptr,double,d->length,"Unable to allocate a new array\n");
  do{
    fscanf(fp,"%le%c",&val,&c);
    ptr[i++]=val;
  }while(c==' ');
  d->v=ptr;
  MYALLOC(ptr,double,d->length,"Unable to allocate a new array\n");
  i=0;
  while(fscanf(fp,"%le%c",&val,&c)!=EOF)
    ptr[i++]=val;
  d->a=ptr;
  return d;
}

int sumViRi(Param p,gsl_vector * V){
  double sum=0;
  for(int i=0; i < p->b; i++)
    sum+=gsl_vector_get(V,i)/p->R[i];
  return sum;
}

double * solve (double * A,double *B,int size){
  gsl_matrix_view m = gsl_matrix_view_array (A, size, size);
  gsl_vector_view b = gsl_vector_view_array (B, size);
  gsl_permutation *p = gsl_permutation_alloc (size);
  gsl_vector *x = gsl_vector_alloc (size);
  double *X;
  int s;
  MYALLOC(X,double,size,"Unable to allocate solution vector\n");
  
  gsl_linalg_LU_decomp (&m.matrix, p, &s);
  gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x);

  memcpy(X,x->data,sizeof(double)*size);

  gsl_permutation_free (p);
  gsl_vector_free (x);
  return X;
}

gsl_vector * getVector(double * v,int n){
  gsl_vector *gv = gsl_vector_alloc(n);
  for(int k=0;k<n;k++)
    gsl_vector_set(gv,k,v[k]);
  return gv;
}

void freeVector(gsl_vector * v){
  v->owner=1;
  gsl_vector_free(v);
}

int findIndex(double *vec,int size,double val){
  for(int k=0; k<size ; k++)
    if(vec[k]>val)
      return k;
  fprintf(stderr,"FindIndex failed\n");
  return -1;
}

double getCk(Data set, Param p, int k){
  int isucc,iprec;
  double C;
  isucc=findIndex(set->t,set->length,p->Tau[k]);
  iprec=findIndex(set->t,set->length,p->Tau[k-1]);
  C=0;
  for(int i=0; i<=k-1 ; i++)
    C+=p->C[i];

#ifdef DEBUG
  printf("k=%d: sum(C(i))=%.3lf\tiprec=%d\tisucc=%d\n",k,C,iprec,isucc);
#endif
  
  return (C*(set->v[iprec] - set->v[isucc]) / set->v[isucc])
    + p->Cv*((pow(set->v[iprec],2))-(pow(set->v[isucc],2)))
    / (2*set->v[isucc]);
}

double getRiso(Data set,Param p){
  int iprec,isucc;
  double C;
  isucc=findIndex(set->t,set->length,p->Tau[p->b]);
  iprec=findIndex(set->t,set->length,p->Tau[p->b-1]);
  
  C=0;
  for(int i=0; i<p->b ; i++)
    C+=p->C[i];

  return (set->t[iprec]-set->t[isucc])
    / ((C+(set->v[isucc]-set->v[iprec])/2*p->Cv) * log(set->v[isucc]/set->v[iprec]));
}

Param identification(Data set,int b,int m){
  Param p;
  MYALLOC(p,Sys,1,"Unable to allocate System structure\n");
  MYALLOC(p->R,double,b+1,"Unable to allocate resistances' vector\n");
  MYALLOC(p->C,double,b,"Unable to allocate capacitances' vector\n");
  MYALLOC(p->Tau,double,b+1,"Unable to allocate time constants' vector\n");
  p->b=b;
  //manual parameter initialization for first branch
  p->Cv=100;
  p->C[0]=5.78;
  p->R[0]=0.04;
  p->Tau[0]= (p->C[0] + set->v[0]*p->Cv) * p->R[0];

  for(int k=1; k<b ; k++){
    p->Tau[k]= m * p->Tau[k-1];
    p->C[k]=getCk(set,p,k);
    p->R[k]=p->Tau[k] / p->C[k];
  }
  p->R[b]=getRiso(set,p);
  return p;
}

double getRpar(Param p){
  double rp=0;
  for(int k=0; k <= p->b ;k++) //compute also Riso = R[b]
    rp+=1/p->R[k];
  return 1/rp;
}

gsl_matrix * dynMatrix(Param p){
  gsl_matrix * A=gsl_matrix_alloc(p->b,p->b);
  double Rpar=getRpar(p);
  for(int i=0;i<p->b;i++)
    for(int j=0;j<p->b;j++)
      if(i==j)
	gsl_matrix_set(A,i,j,(Rpar/(p->R[i])-1)/(p->R[i]*p->C[i]));
      else
	gsl_matrix_set(A,i,j,Rpar/(p->R[i]*p->R[j]*p->C[i]));
  return A;
}

gsl_vector * bVec(Param p){
  gsl_vector *b = gsl_vector_alloc(p->b);
  double Rpar=getRpar(p);
  for(int i=0;i<p->b;i++)
    gsl_vector_set(b,i,Rpar/(p->R[i]*p->C[i]));
  return b;
}

gsl_vector * cVec(Param p){
  gsl_vector *c = gsl_vector_alloc(p->b);
  double Rpar=getRpar(p);
  for(int i=0; i<p->b; i++)
    gsl_vector_set(c,i,Rpar/p->R[i]);
  return c;
}

double * simulate(Param p,Data set,double *iCond){
  double T,*Vx,C_,Rpar;
  gsl_matrix *I = gsl_matrix_alloc(p->b,p->b),
             *A = gsl_matrix_alloc(p->b,p->b),
            *A1 = gsl_matrix_alloc(p->b,p->b),
         *invA1 = gsl_matrix_alloc(p->b,p->b);
  gsl_vector *b = gsl_vector_alloc(p->b),
            *b1 = gsl_vector_alloc(p->b),
         *state = gsl_vector_alloc(p->b);
  gsl_permutation *perm = gsl_permutation_alloc (p->b);
  int s;
  
  T=set->t[1]-set->t[0];
  
  MYALLOC(Vx,double,set->length,"Unable to allocate output vector\n");

  Vx[0]=set->v[0]; //initial conditions
  for(int k=0; k<p->b ; k++)
    gsl_vector_set(state,k,iCond[k]);
  C_=p->C[0]; //backup c1
  Rpar=getRpar(p);
  gsl_matrix_set_identity(I);

  for(int k=1; k< set->length ; k++){
    p->C[0] = C_+p->Cv*Vx[k-1]; //update voltage dependent capacitance
    A=dynMatrix(p);//calculate dynamic matrix
    b=bVec(p); //calculate input vector

    if(k==1)printGSLmatrix(A,p->b,"A"); //debug
    gsl_matrix_scale(A,T/2); //scale A
    //    if(k==1)printGSLmatrix(A,p->b,"A*T/2"); //debug
    gsl_matrix_memcpy(A1,A); //duplicate scaled A
    gsl_matrix_add(A,I);     //add identity to scaled A (A=I+T/2*A)
    if(k==1)printGSLmatrix(A,p->b,"OK I+T/2*A"); //debug
    gsl_matrix_scale(A1,-1); //build subtraction
    gsl_matrix_add(A1,I);    //subtract scaled A to identity (A1=I-T/2*A )
    if(k==1)printGSLmatrix(A1,p->b,"OK I-T/2*A"); //debug
    gsl_linalg_LU_decomp(A1,perm,&s); //decompose A1
    gsl_linalg_LU_invert(A1,perm,invA1); //invert A1
    if(k==1)printGSLmatrix(invA1,p->b,"OK inv(I-A*T/2)"); //debug
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,invA1,A,0,A1); //calculate final A1=invA1*A
    if(k==1)printGSLmatrix(A1,p->b,"OK Adisc"); //debug
    gsl_blas_dgemv(CblasNoTrans,T/2,invA1,b,0,b1); //calculate final b1=invA1*b*T/2
    if(k==1)printGSLvector(b,p->b,"OK b");
    if(k==1)printGSLvector(b1,p->b,"OK inv(ImAT2)*b*T/2");
    if(k==1)printGSLvector(state,p->b,"state");
    gsl_blas_dgemv(CblasNoTrans,1,A1,state,set->a[k]+set->a[k-1],b1); //calculate new state in b1
    gsl_vector_memcpy(state,b1); //save new state in state vector
    if(k==1)printGSLvector(state,p->b,"new state");

    
    Vx[k]=sumViRi(p,state)*Rpar + Rpar*set->a[k]; //calculate output
  }
  p->C[0]=C_; //restore c1

#ifdef DEBUG
  printf("T=%.3lf\nCv=%.3lf\n",T,p->Cv);
  printVector(p->R,p->b+1,"R");
  printVector(p->C,p->b,"C");
  printVector(p->Tau,p->b,"Tau");
  printGSLmatrix(I,p->b,"I");
#endif
  
  return Vx;
}

int main(int argc,char ** argv) {
  //declarations
  FILE *fp1,*fp2;
  Data set1,set2;
  h_GPC_Plot *plot;
  double *Vx,initCond[BRANCH]={2.742866,2.284812,0.259608,0.015110};
  Param param;

  //arguments check
  if(argc != 3){
    printf("Usage: %s dataFile1 dataFile2\n",argv[0]);
    return 0;
  }

  //data retrivial
  if((fp1=fopen(argv[1],"r"))==NULL){
    printf("%s not found\n",argv[1]);
    return 0;
  }
  if((fp2=fopen(argv[2],"r"))==NULL){
    printf("%s not found\n",argv[2]);
    return 0;
  }
  set1=getData(fp1);
  set2=getData(fp2);
  fclose(fp1);
  fclose(fp2);

  //simulation
  param = identification(set1,BRANCH,EMM);
  Vx=simulate(param,set2,initCond);
	     
  //presentation
  plot = gpc_init_2d("Simulation","Time [s]","Voltage [V]",
		     GPC_AUTO_SCALE,GPC_POSITIVE,GPC_KEY_ENABLE);
  if(plot == NULL)
    fprintf(stderr,"Unable to open new plot window\n");
  
  gpc_plot_2d(plot,Vx,(set1->length),"set1",set1->t[0],
	      set1->t[set1->length-1],"lines","blue",GPC_NEW);
  printf("Press enter to end the program...\n>>> ");
  getchar();
  gpc_close(plot);
  
  //automatic free for Param and Data structures
return 0;
}

