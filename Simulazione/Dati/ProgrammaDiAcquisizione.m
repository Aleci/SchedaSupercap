graphics_toolkit('gnuplot')
pkg load arduino
%arduinosetup;

interrupt = 'd7';
vmeter = 'a4';
ammeter = 'a3';
led = 'd13';
T_sampling = .01;
R_sense = 2.5;

mcu=arduino;
volt=[readVoltage(mcu,vmeter)];
amp=[readVoltage(mcu,ammeter)];
t=[0];
writeDigitalPin(mcu,led,0);
while(readDigitalPin(mcu,interrupt))
  a=readVoltage(mcu,ammeter);
  v=readVoltage(mcu,vmeter);
  volt=[volt,v];
  amp=[amp,(a-v)/R_sense];
  t=[t,t(end)+T_sampling];
  pause(T_sampling);
endwhile
writeDigitalPin(mcu,led,0);

function seqLP=lpf (seq,n)
  seqLP=[];
  for i = 1:length(seq)
    if(length(seq)>i+n)
      seqLP(i)=sum(seq(i:i+n-1))./n;
    else
      seqLP(i)=seqLP(i-1);
    endif
  end
endfunction

if(length(volt)>11)
  voltLP = lpf(volt,10);
endif
