1; %This is a part of script common to both simulators

%Get the matrix representation of the system from C, R, Tau vectors
function [A B C D]=getSystem(C,R,Tau)
  rp=getRpar(R);
  maxK=length(C);
  for(i=1:maxK)
    for(j=1:maxK)
      if(i!=j)
	A(i,j)=rp/(Tau(i)*R(j));
      else
	A(i,j)=(rp/R(j)-1)/Tau(j);
      endif
    end
  end
  for(i=1:maxK)
    B(i)=rp/Tau(i);
  end
  B=B';
  for(i=1:maxK)
    C(i)=rp/R(i);
  end
  D=rp;
endfunction

%Apply charge propagation method to identify other branches
%t and v should be trimmed before the call
function [C R Tau]=identifyOther(t,v,c1,cv,r1,m,maxK)
  Tau(1) = (c1+2.7*cv)*r1;
  C(1)=c1;
  R(1)=r1;
  for k=2:maxK
    Tau(k) = m*Tau(k-1);
    C(k)=getCk(t,v,C,cv,Tau,k);
    R(k)=Tau(k)/C(k);
  end
  R(maxK+1)=getRiso(t,v,C,cv,Tau);
endfunction

%Calculate kth capacitance
function Ck=getCk(t,v,c,cv,tau,k)
  isucc=find(t>=tau(k))(1)
  iprec=find(t>=tau(k-1))(1)
  C=sum(c(1:k-1))
  Ck=(C*(v(iprec)-v(isucc))/v(isucc)) + (cv*((v(iprec)^2)-(v(isucc)^2))/(2*v(isucc)))
endfunction

%Calculate leakage resistance
function Riso=getRiso(t,v,c,cv,tau)
  iprec=find(t>=tau(end))(1);
  Riso=(t(iprec)-t(end))/((sum(c)+(v(end)-v(iprec))/2*cv)*log(v(end)/v(iprec)));
endfunction

%Calculate Rpsi
function Rpsi=getRpsi(R,n)
  Rpsi=1/sum(1./R(1:n));
endfunction

%Calculate parallel equivalent resistance
function Rpar=getRpar(R)
  Rpar=1/sum(1./R);
endfunction

%Integrate sequence using Tustin method
function intNum=tustin(seq,T)
  intNum(1)=0;
  for i=2:length(seq)
    intNum(i)=(seq(i)+seq(i-1))*T/2;
  end
endfunction
